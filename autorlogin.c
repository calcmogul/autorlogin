/* autorlogin: Starts rlogin with given arguments. rlogin is restarted
   each time an IPC message notification is received. */

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define AUTORLOGIN_UNPATH "/var/run/autorlogin.socket"
#define RLOGIN_PATH "rlogin"
#define DEBUG

/* Spawn an rlogin process */
int spawnrlogin(int argc, char **argv, int *cpid, int sd);

/* Receive a datagram from a UNIX SOCK_STREAM socket */
int recv_ipc(int sd, void *buf, size_t len);

int
main(int argc, char **argv)
{
  char buf[128];
  const int bufsize = 128;

  struct sockaddr_un addr;

  int sd;
  int ok;
  int uid;
  int olduid;

  /* The child pid */
  int pid;

  pid = 0;

  /* Bad args */
  if(argc < 2) {
    fprintf(stderr, "autorlogin: autorlogin [rlogin args]\n");
    exit(1);
  }

  /* Handle the SIGCHLD signal */
  if(signal(SIGCHLD, SIG_IGN) == SIG_ERR) {
    /* fprintf(stderr, "%s: signal(2) failed\n", argv[0]); */
    perror(argv[0]);
    exit(1);
  }

  /* Create a AF_UNIX socket */
  sd = socket(AF_UNIX, SOCK_STREAM, 0);
  if(sd < 0) {
    perror(argv[0]);
    exit(1);
  }

  /* Connect to the autorlogind process */
  addr.sun_family = AF_UNIX;
  strcpy(addr.sun_path, AUTORLOGIN_UNPATH);
  ok = connect(sd, (struct sockaddr *) &addr, sizeof(struct sockaddr_un));
  if(ok != 0) {
    perror(argv[0]);
    exit(1);
  }

  /* Spawn an initial rlogin which will work if rlogind
     is already started. If it isn't, the process will
     be killed by the main loop. */
#ifdef DEBUG
  fprintf(stderr, "%s: starting rlogin\r\n", argv[0]);
#endif
  ok = spawnrlogin(argc, argv, &pid, sd);
  if(ok != 0) {
    close(sd);
    perror(argv[0]);
  }

  uid = 0;
  olduid = 0;

  /* The main loop restarts rlogin whenever a UDP
     broadcast packet is received on the port
     specified by AUTORLOGIN_PORT . */
  while(1) {
    /* Wait for a multicast packet from a new session */
    while(uid == olduid) {
      ok = recv_ipc(sd, buf, bufsize);
      if(ok == -1) {
        perror(argv[0]);
        exit(1);
      }

      memcpy(&uid, buf, 4);
#ifdef DEBUG
      fprintf(stderr, "%s: uid=0x%08x\r\n", argv[0], uid);
#endif
    }
    olduid = uid;

    /* Kill the child process if it exits. */
    if(pid) {
#ifdef DEBUG
      fprintf(stderr, "%s: killing rlogin\r\n", argv[0]);
#endif
      kill(pid, SIGKILL);
      pid = 0;
    }

    /* Spawn the rlogin process */
#ifdef DEBUG
    fprintf(stderr, "%s: spawning rlogin\r\n", argv[0]);
#endif
    ok = spawnrlogin(argc, argv, &pid, sd);
    if(ok != 0) {
      close(sd);
      perror(argv[0]);
    }
  }

  close(sd);
  return 0;
}

/* Spawn an rlogin process */
int
spawnrlogin(int argc, char **argv, int *cpid, int sd)
{
  char *args[128];
  int i;

  *cpid = fork();
  if(*cpid == -1) {
    return -1;
  }
  if(*cpid == 0) {
    close(sd);

    /* Set up args[0] to the binary path. */
    args[0] = RLOGIN_PATH;

    /* Set up the arguments to pass to rlogin */
    for(i = 1; i < argc && i < 128; i++) {
      args[i] = argv[i];
    }
    args[i] = 0;


    execvp(RLOGIN_PATH, args);
    exit(1);
  }

  return 0;
}

int
recv_ipc(int sd, void *buf, size_t len)
{
  int msgsize;
  int nrecvd;

  nrecvd = recv(sd, &msgsize, sizeof(int), 0);
  if(msgsize > len) return -1;

  nrecvd = recv(sd, buf, msgsize, 0);
  if(nrecvd != msgsize) return -1;

  return nrecvd;
}
