/* autorlogind: Notifies connected processes of UDP packets received
   on port 35120. */

#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/stat.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <netinet/udp.h>

#define AUTORLOGIN_PORT 35120
#define AUTORLOGIN_UNPATH "/var/run/autorlogin.socket"
#define AUTORLOGIN_BUFSIZE 128
#define DEBUG

#define max(a, b) ((a) > (b) ? (a) : (b))

/* A linked list of connected sockets */
struct socketlist_t {
  int sd;
  struct socketlist_t *next;
  struct socketlist_t *prev;
};

/* Close a socket in a socketlist_t, removing it from the
   list and freeing the list structure. /list/ points to
   the list node to close and free. /top/ is a double pointer
   to the first node in the list, which will be changed if
   /list/ is the first node in the list. */
void
close_list_socket(struct socketlist_t *list, struct socketlist_t **top)
{
  if(list->prev != NULL)
    list->prev->next = list->next;
  else
    *top = list->next;

  if(list->next != NULL)
    list->next->prev = list->prev;
  close(list->sd);
  free(list);

  return;
}

/* Create a UNIX domain socket listening at the specified path. */
int
listen_unix(char *path)
{
  int ok;
  int sd;
  struct sockaddr_un addr;

  /* Ensure the socket does not exist */
  unlink(path);

  /* Create a socket */
  sd = socket(AF_UNIX, SOCK_STREAM, 0);
  if(sd < 0) {
    return -1;
  }

  /* Bind the socket */
  addr.sun_family = AF_UNIX; 
  strcpy(addr.sun_path, path);
  ok = bind(sd, (struct sockaddr *) &addr, sizeof(struct sockaddr_un));
  if(ok != 0) {
    close(sd);
    return -1;
  }

  /* Listen for connections */
  ok = listen(sd, 5);
  if(ok != 0) {
    close(sd);
    return -1;
  }

  /* Change the permissions on the socket */
  ok = chmod(path, 666);
  if(ok != 0) {
    close(sd);
    return -1;
  }

  return sd;
}

/* Create a UDP socket listening on the specified port. */
int
listen_udp(unsigned short port)
{
  int ok;
  int sd;
  struct sockaddr_in addr;

  /* Create the socket to receive UDP broadcast
     datagrams on. */
  sd = socket(AF_INET, SOCK_DGRAM, 0);
  if(sd < 0) {
    return -1;
  }

  /* Bind the socket to the correct port */
  memset(&addr, 0, sizeof(struct sockaddr_in));
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);
  addr.sin_addr.s_addr = htonl(INADDR_ANY);
  ok = bind(sd, (struct sockaddr *) &addr, sizeof(struct sockaddr_in));
  if(ok != 0) {
    close(sd);
    return -1;
  }

  return sd;
}

int
main(int argc, char **argv)
{
  int ok;

  int udpsd, unixsd;

  fd_set readfds, writefds, exceptfds;
  int nfds;

  char buf[AUTORLOGIN_BUFSIZE];
  int nrecvd;

  int newsd;
  struct sockaddr_un unaddr;
  socklen_t addrsize;

  struct socketlist_t *list = NULL;
  struct socketlist_t *tmplist;
  struct socketlist_t *next;

  FILE *pidfp;

  /* Options */
  int option;
  int nofork;
  char *pidfile;

  /* Parse options from the command line */
  nofork = 0;
  pidfile = NULL;

  while((option = getopt(argc, argv, "dp:")) != -1) {
    switch(option) {
    case 'd':
      nofork = 1;
      break;

    case 'p':
      pidfile = optarg;
      break;

    case '?':
    case ':':
      fprintf(stderr, "%s: autorlogind [-d] [-p pidfile]\n", argv[0]);
      exit(1);
    }
  }

  /* Listen on the UDP port */
  udpsd = listen_udp(AUTORLOGIN_PORT);
  if(udpsd == -1) { 
    perror(argv[0]);
    exit(1);
  }

  /* Listen on the UNIX socket */
  unixsd = listen_unix(AUTORLOGIN_UNPATH);
  if(unixsd == -1) {
    perror(argv[0]);
    exit(1);
  }

#ifdef DEBUG
  fprintf(stderr, "%s: Listening on %s\n", argv[0], AUTORLOGIN_UNPATH);
#endif

  /* Ignore SIGPIPE */
  if(signal(SIGPIPE, SIG_IGN) == SIG_ERR) {
    perror(argv[0]);
    exit(1);
  }

  /* Fork into the background */
  if(!nofork) {
    if(fork() != 0) {
      exit(0);
    }
  }

  /* Write out the pidfile */
  if(pidfile != NULL) {
    pidfp = fopen(pidfile, "w");
    fprintf(pidfp, "%d\n", getpid());
    fclose(pidfp);
  }

  /* select(2) loop */
  while(1) {
    /* Clear the fd_set s */
    FD_ZERO(&readfds);
    FD_ZERO(&writefds);
    FD_ZERO(&exceptfds);

    /* Set the UDP socket into the bitfield */
    FD_SET(udpsd, &readfds);
    nfds = udpsd;

    /* Set the UNIX socket listener into the bitfield */
    FD_SET(unixsd, &readfds);
    nfds = max(nfds, unixsd);

    /* select(2) on reading and exceptions for connected clients */
    for(tmplist = list; tmplist != NULL; tmplist = tmplist->next) {
      FD_SET(tmplist->sd, &readfds);
      FD_SET(tmplist->sd, &exceptfds);
    }

    /* Engage! */
    ok = select(nfds+1, &readfds, &writefds, &exceptfds, NULL);
    if(ok == -1) {
      perror(argv[0]);
      exit(1);
    }

    /* Check for new UDP packets */
    if(FD_ISSET(udpsd, &readfds)) {
      /* Clear out the buffer */
      memset(buf, 0, AUTORLOGIN_BUFSIZE);

      /* Receive a UDP packet */
      nrecvd = recv(udpsd, buf, AUTORLOGIN_BUFSIZE, 0);
      if(nrecvd < 0) {
        perror(argv[0]);
        exit(1);
      }

      /* Send it to the connected clients */
      for(tmplist = list; tmplist != NULL; tmplist = next) {
        next = tmplist->next;
        ok = send(tmplist->sd, &nrecvd, sizeof(int), 0);
        ok = send(tmplist->sd, buf, nrecvd, 0);
        if(ok != nrecvd) {
          close_list_socket(tmplist, &list);
        }else{
        }
      }
    }

    /* Check for new connections */
    if(FD_ISSET(unixsd, &readfds)) {
      /* Accept the connection */
      addrsize = 0;
      newsd = accept(unixsd, (struct sockaddr *) &unaddr,
        &addrsize);
      if(newsd == -1) {
        perror(argv[0]);
        exit(1);
      }

#ifdef DEBUG
      fprintf(stderr, "%s: Connection accepted\n", argv[0]);
#endif

      /* Add the descriptor to the list */
      tmplist = malloc(sizeof(struct socketlist_t));
      tmplist->next = list;
      tmplist->prev = NULL;
      tmplist->sd = newsd;
      if(list != NULL)
        list->prev = tmplist;
      list = tmplist;
    }

    /* Check the selected sockets */
    for(tmplist = list; tmplist != NULL; tmplist = next) {
      next = tmplist->next;

      /* If a connected client selects on reading,
         receive the data and ignore it. */
      if(FD_ISSET(tmplist->sd, &readfds)) {
        ok = recv(tmplist->sd, buf, 128, 0);
        if(ok < 1) {
          close_list_socket(tmplist, &list);
        }else{
        }
      }

      /* If an exception occurred on a socket,
         disconnect it. */
      if(FD_ISSET(tmplist->sd, &exceptfds)) {
        close_list_socket(tmplist, &list);
      }
    }

  }

  return 0;
}

