all: autorlogin autorlogind

autorlogin: autorlogin.o
	gcc $(LDFLAGS) -o $@ $<

autorlogind: autorlogind.o
	gcc $(LDFLAGS) -o $@ $<

%.o: %.c
	gcc $(CFLAGS) -c -o $@ $<

clean:
	-$(RM) -r *.o autorlogin autorlogind

install:
	mkdir -p "$(DESTDIR)/usr/bin"
	mkdir -p "$(DESTDIR)/usr/lib/systemd/system"

	install -m755 autorlogin "$(DESTDIR)/usr/bin/autorlogin"
	install -m755 autorlogind "$(DESTDIR)/usr/bin/autorlogind"
	install -m644 autorlogind.service "$(DESTDIR)/usr/lib/systemd/system/autorlogind.service"
